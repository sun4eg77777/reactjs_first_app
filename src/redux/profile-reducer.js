const ADD_POST = 'ADD-POST';
const UPDATE_NEW_POST_TEXT = 'UPDATE-NEW-POST-TEXT';

let initialstate = {
    posts: [
        {id: 1, message: 'Hi, how are you?', likescount: 12},
        {id: 2, message: 'It"s my first post', likescount: 23},
        {id: 3, message: 'My message for all', likescount: 23},
        {id: 4, message: 'Hello', likescount: 23},
    ],
    newPostText: '',
};

const profileReducer = (state = initialstate, action) => {

    switch (action.type) {
        case ADD_POST:
            let newPost = {
                id: 5,
                message: state.newPostText,
                likescount: 0,
            };
            state.posts.push(newPost);
            state.newPostText = '';
            return state;
        case UPDATE_NEW_POST_TEXT:
            state.newPostText = action.newText;
            return state;
        default:
            return state;
    }

}

export const addPostActionCreator = () => ({type: ADD_POST});
export const updateNewPostTextActionCreator = (text) =>
    ({type: UPDATE_NEW_POST_TEXT, newText: text});


export default profileReducer;