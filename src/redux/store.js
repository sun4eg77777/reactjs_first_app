import profileReducer from "./profile-reducer";
import dialogsReducer from "./dialogs-reducer";
import sidebarReducer from "./sidebar";

let store = {
    _state: {
        profilePage: {
            posts: [
                {id: 1, message: 'Hi, how are you?', likescount:12},
                {id: 2, message: 'It"s my first post', likescount:23},
                {id: 3, message: 'My message for all', likescount:23},
                {id: 4, message: 'Hello', likescount:23},
            ],
            newPostText: '',
        },
        dialogsPage: {
            dialogs: [
                {id: 1, name: 'Dima'},
                {id: 2, name: 'Andy'},
                {id: 3, name: 'Bohdan'},
                {id: 4, name: 'Alex'},
                {id: 5, name: 'Julia'},
                {id: 6, name: 'Sergey'},
            ],
            messages: [
                {id: 1, message: 'Hi'},
                {id: 2, message: 'How is your react?'},
                {id: 3, message: 'Yo'},
                {id: 4, message: 'Yo'},
                {id: 5, message: 'Yo'},
            ],
            newMessageBody: '',
        },
        sidebar: {},
    },
    _callSubscriber() {
        console.log('State was changed');
    },

    getState() {
        return this._state;
    },
    subscribe (observer) {
        this._callSubscriber = observer;
    },

    dispatch(action){ // {  type: 'ADD-POST' }

        this._state.profilePage = profileReducer(this._state.profilePage , action);
        this._state.dialogsPage = dialogsReducer(this._state.dialogsPage , action);
        this._state.sidebar = sidebarReducer(this._state.sidebar, action);

        this._callSubscriber(this._state);
    }
}



export default store;
window.state = store;
// store - OOP