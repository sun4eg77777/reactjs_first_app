import React from 'react';
import s from './Post.module.css';

const Post = (props) => {
    return (
        <div>
            <div className={s.item}>
                <img src='https://hornews.com/images/news_large/c1d4b2b8ec608ea72764c5678816d5c9.jpg' alt='avatar' />
                <p className={s.text}>{props.message}</p>
                <div className={s.socInfo}>
                    <span>like</span> {props.likesCount}
                </div>
            </div>
        </div>
    );
}

export default Post;