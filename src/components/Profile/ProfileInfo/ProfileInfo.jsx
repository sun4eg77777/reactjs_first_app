import React from 'react';
import s from './ProfileInfo.module.css';

const ProfileInfo = () => {
    return (
        <div>
            <div className={s.cover}>
                <img src='https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?h=350' alt='cover'/>
            </div>
            <div className={s.descriptionBlock}>
                ava + description
            </div>
        </div>
    );
}

export default ProfileInfo;